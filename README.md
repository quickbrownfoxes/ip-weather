# IP and Weather Webpage
A simple IP address checker and weather information using Google Maps and Darksky APIs. To get started all you need is both the maps API key and the Darksky API key and put them at the top of the file!. 

## Usage
    <?php 
    //API Keys
    $darkskyapi= "your_api_key_here";
    $googlemapsapi = "your_api_key_here";
    ?>

*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*
